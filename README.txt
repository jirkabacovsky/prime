Sample application for reading xlsx data source file and printing its prime number to the console.
Application will ask for data source file if not provided as program startup arguments.
If file cannot be found or read message will be printed to the console.

To run the application use prebuilded prime.jar or build it from sources:
1. java -jar prime.jar
2. java -jar prime.jar --prime.filename=pathToTheFile