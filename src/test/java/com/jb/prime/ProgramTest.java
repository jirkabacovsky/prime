package com.jb.prime;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.jb.prime.app.Program;
import com.jb.prime.app.Properties;
import com.jb.prime.services.DataSourceParser;
import com.jb.prime.services.PrinterService;

@RunWith(MockitoJUnitRunner.class)
public class ProgramTest {

	@Mock
	private Properties properties;
	
	@Mock
	private PrinterService printerService;

	@Mock
	private DataSourceParser dataSourceParser;

	@InjectMocks
	private Program service = new Program(properties, printerService, dataSourceParser);

	@Test
	public void thatPrimeIsPrinted() throws Exception{
		String fileName = "pathToFile";
		when(properties.getFilename()).thenReturn(fileName);
		String[] stringArray = new String[] {"1","2","3","4"};
		when(dataSourceParser.getStringValuesFromDataSource(fileName)).thenReturn(Arrays.asList(stringArray));
		
		service.afterPropertiesSet();

		verify(printerService, times(4)).printPrime(anyString());
	}

}
