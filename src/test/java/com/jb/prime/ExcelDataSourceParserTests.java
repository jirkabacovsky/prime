package com.jb.prime;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.junit.Test;

import com.jb.prime.services.impls.ExcelDataSourceParserImpl;

public class ExcelDataSourceParserTests {

	@Test
	public void thatResultI12() {
		ExcelDataSourceParserImpl service = new ExcelDataSourceParserImpl();
		
		List<String> result = service.getStringValuesFromDataSource("vzorek_dat_test.xlsx");
		
		assertEquals(12, result.size());
	}
	
	@Test
	public void thatFileDoesntExists() {
		ExcelDataSourceParserImpl service = new ExcelDataSourceParserImpl();
		
		List<String> result = service.getStringValuesFromDataSource("jatunejsem.xlxs");

		assertEquals(0, result.size());

	}
}
