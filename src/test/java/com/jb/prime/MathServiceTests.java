package com.jb.prime;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.jb.prime.services.impls.SimpleMathServiceImpl;

public class MathServiceTests {

	SimpleMathServiceImpl service = new SimpleMathServiceImpl();

	@Test
	public void thatNotANumber() {
		boolean prime1 = service.isPrime(1);
		assertEquals(false, prime1);
		
		boolean prime2 = service.isPrime(2);
		assertEquals(true, prime2);
		
		boolean prime3 = service.isPrime(4);
		assertEquals(false, prime3);
		
		boolean prime4 = service.isPrime(6);
		assertEquals(false, prime4);
		
		boolean prime5 = service.isPrime(7);
		assertEquals(true, prime5);
	}
}
