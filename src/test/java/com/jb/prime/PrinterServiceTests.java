package com.jb.prime;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import com.jb.prime.services.MathService;
import com.jb.prime.services.PrinterService;
import com.jb.prime.services.impls.PrinterServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class PrinterServiceTests {

	@Mock
	private MathService mathService;

	@InjectMocks
	private PrinterService printerService = new PrinterServiceImpl(mathService);

	@Test
	public void thatPrimeIsPrinted() {
		printerService.printPrime("3");

		verify(mathService, times(1)).isPrime(anyLong());
	}

	@Test
	public void thatNotANumber() {
		printerService.printPrime("xxx");

		verify(mathService, times(0)).isPrime(anyLong());
	}
}
