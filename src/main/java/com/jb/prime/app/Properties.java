package com.jb.prime.app;

import javax.validation.constraints.NotBlank;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

/**
 * storage of the application properties
 * @author bacovjir
 *
 */
@Component
@ConfigurationProperties(prefix = "prime")
@Validated
public class Properties {

	@NotBlank
	private String filename;

	public void setFilename(String filename) {
		this.filename = filename;
	}

	public String getFilename() {
		return filename;
	}
	
}
