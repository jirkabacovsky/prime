package com.jb.prime.app;

import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import com.jb.prime.services.DataSourceParser;
import com.jb.prime.services.PrinterService;

/**
 * Main class to organize the app logic
 * @author Jiří Bačovský
 *  
 */
@Component
public class Program implements InitializingBean{

	private Properties properties;

	private PrinterService printerService;

	private DataSourceParser dataSourceParser;

	public Program(Properties properties, PrinterService printerService,
			DataSourceParser dataSourceParser) {
		this.properties = properties;
		this.printerService = printerService;
		this.dataSourceParser = dataSourceParser;
	}

	@Override
	public void afterPropertiesSet() throws Exception {		
		List<String> allStringValues = dataSourceParser.getStringValuesFromDataSource(properties.getFilename());

		allStringValues.forEach( stringValue ->
			printerService.printPrime(stringValue)
		);
	}

}
