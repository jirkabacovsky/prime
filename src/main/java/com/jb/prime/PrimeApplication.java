package com.jb.prime;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.jb.prime.services.impls.ArgReader;

/**
 * Main spring boot app entry point
 * @author Jiří Bačovský
 *
 */
@SpringBootApplication
public class PrimeApplication{

	public static void main(String[] args) {
		args = ArgReader.getParams(args);
		SpringApplication.run(PrimeApplication.class, args);
	}
}
