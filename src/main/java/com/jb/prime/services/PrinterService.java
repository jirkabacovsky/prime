package com.jb.prime.services;

/**
 * class for handling printing of the value to console 
 * @author Jiří Bačovský
 *
 */
public interface PrinterService {

	public void printPrime(String stringValue);
}
