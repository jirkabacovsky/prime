package com.jb.prime.services;

/**
 * defines methods meth operations
 * @author Jiří Bačovský
 *
 */
public interface MathService {

	/**
	 * check if provided number is prime number
	 * @param number is number under investigation
	 * @return true if number is prime otherwise false
	 */
	public boolean isPrime(long number);
}
