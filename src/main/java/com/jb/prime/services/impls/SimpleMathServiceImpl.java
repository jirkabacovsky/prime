package com.jb.prime.services.impls;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.jb.prime.services.MathService;

/**
 * One of the prime number algorithm
 * @author Jiří Bačovský
 *
 */
@Service
@Profile("default")
public class SimpleMathServiceImpl implements MathService{

	@Override
	public boolean isPrime(long num) {
		if (num < 2) return false;
		if (num == 2) return true;
		if (num % 2 == 0) return false;
		for (int i = 3; i * i <= num; i += 2)
			if (num % i == 0) return false;
		return true;
	}

}
