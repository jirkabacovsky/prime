package com.jb.prime.services.impls;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.jb.prime.services.MathService;
import com.jb.prime.services.PrinterService;

/**
 * Parse the string value to number and writes it to console if its prime number
 * @author Jiří Bačovský
 *
 */
@Service
public class PrinterServiceImpl implements PrinterService {

	protected static final Logger log = LoggerFactory.getLogger(PrinterServiceImpl.class);

	private MathService mathService;

	public PrinterServiceImpl(MathService mathService) {
		super();
		this.mathService = mathService;
	}

	@Override
	public void printPrime(String stringValue) {
		try {
			long number = Long.parseLong(stringValue);
			boolean isPrime = mathService.isPrime(number);
			if (isPrime) {
				System.out.println(number);
			}
		} catch (NumberFormatException nfe) {
			log.error("{} is not a number", stringValue);
		}
	}

}
