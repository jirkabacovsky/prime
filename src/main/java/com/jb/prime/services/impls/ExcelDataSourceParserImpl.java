package com.jb.prime.services.impls;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.jb.prime.services.DataSourceParser;

/**
 * Excel datasource parser implementation
 * @author Jiří Bačovský
 *
 */
@Service
@Profile("excel")
public class ExcelDataSourceParserImpl implements DataSourceParser {

	private static final Logger log = LoggerFactory.getLogger(ExcelDataSourceParserImpl.class);

	@Override
	public List<String> getStringValuesFromDataSource(String fileName) {
		List<String> result = new ArrayList<>();

		try (FileInputStream excelFile = new FileInputStream(new File(fileName));
				Workbook workbook = new XSSFWorkbook(excelFile);
				) {
			XSSFSheet sheet = (XSSFSheet) workbook.getSheetAt(0);
			Iterator<Row> rowIterator = sheet.iterator();

			while (rowIterator.hasNext()) {
				addCellValueToResult(result, rowIterator.next().getCell(1));
			}

		} catch (FileNotFoundException e) {
			log.error("File {} not found.", fileName);
			System.err.println( String.format("Vypadá to že datový soubor '%s' nebyl nalezen.", fileName) );
		} catch (IOException ioe) {
			log.error("I/O exception of some sort has occurred. {}", ioe);
			System.err.println( String.format("Chyba při čtení souboru '%s'.", fileName) );
		} catch (Exception e) {
			System.err.println("Kolečko se polámalo.");
			log.error("Some funny exception occured. {}", e);
		}
		return result;
	}

	private void addCellValueToResult(List<String> result, Cell cell) {
		//only text fields should be handled
		if (cell.getCellTypeEnum() == CellType.STRING) {
			String stringValue = cell.getStringCellValue();
			result.add(stringValue);
		}
		// if needed add support for other cell types
	}

}
