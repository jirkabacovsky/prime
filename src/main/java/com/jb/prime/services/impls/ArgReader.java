package com.jb.prime.services.impls;

import java.util.Scanner;

import org.springframework.stereotype.Component;

/**
 * helper class to make sure path to data source file is provided
 * data could be added as startup parameter (--prime.filename) 
 * or by asking user to provide it interactively
 * @author Jiří Bačovský
 *
 */
@Component
public class ArgReader {

	private static final String FILE_PATH_ARG = "--prime.filename";

	private ArgReader() {}

	public static String[] getParams(String[] args) {
		boolean fileIsProvided = isFilePathInArgs(args);

		if (!fileIsProvided) {
			return getUserInput(args);
		}

		return args;
	}

	private static String[] getUserInput(String[] args) {
		String[] updatedArgs = new String[args.length+1];
		Scanner scanner = new Scanner(System.in);
		System.out.print("Zadej cestu k datovému souboru: ");
		String filePath = scanner.next();
		scanner.close();

		updatedArgs[args.length] = FILE_PATH_ARG+"="+filePath;
		System.arraycopy(args, 0, updatedArgs, 0, args.length);
		return updatedArgs;
	}

	private static boolean isFilePathInArgs(String[] args) {
		boolean fileIsProvided = false;
		for (int i = 0; i < args.length; i++) {
			String keyValue = args[i];
			if (keyValue.indexOf(FILE_PATH_ARG) != -1) {
				String[] tokens = keyValue.split("=");
				if (tokens[1].length()>0) {
					fileIsProvided = true;	
				}
			}
		}
		return fileIsProvided;
	}
}
