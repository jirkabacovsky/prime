package com.jb.prime.services.impls;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import com.jb.prime.services.DataSourceParser;

/**
 * 
 * @author Jiří Bačovský
 *
 */
@Service
@Profile("csv")
public class CsvDataSourceParserImpl implements DataSourceParser{

	private static final Logger log = LoggerFactory.getLogger(CsvDataSourceParserImpl.class);

	@Override
	public List<String> getStringValuesFromDataSource(String fileName) {
		List<String> result = new ArrayList<>();

		try (Scanner scvFileStream = new Scanner(new File(fileName))) {

			while (scvFileStream.hasNext()) {
				result.addAll(getListOfStrigValues(scvFileStream.next()));
			}

			return result;
		} catch (FileNotFoundException e) {
			log.error("File {} not found.", fileName);
			String message = String.format("Vypadá to že datový soubor '%s' nebyl nalezen.", fileName);
			System.out.println(message);
		} catch (Exception e) {
			System.out.println("Kde se stala chyba?");
			log.error("Some funny exception occured. {}", e);
		}

		return result;
	}

	private List<String> getListOfStrigValues(String line) {
		String[] values = line.split(",");
		return Arrays.asList(values);
	}

}
