package com.jb.prime.services;

import java.util.List;

/**
 * defines methods to get values from datasource
 * @author Jiří Bačovský
 *
 */
public interface DataSourceParser {

	/**
	 * from provider file returns list of string values
	 * @param fileName is path to the data source file
	 * @return list of string values
	 */
	public List<String> getStringValuesFromDataSource(String fileName);
}
